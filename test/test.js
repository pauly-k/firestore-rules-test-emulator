import assert from 'assert'
import firebase from '@firebase/testing'

const MY_PROJECT_ID = 'pauly-activity'

describe('Our social app', () => {
  it('understand basic adding ', () => {
    assert.equal(5 + 6, 11)
  })

  it('can read from the read-only collection', async () => {
    let db = firebase.initializeTestApp({ projectId: MY_PROJECT_ID}).firestore() 
    let testDoc = db.collection('readonly').doc('testdoc')
    await firebase.assertSucceeds(testDoc.get())
  })

  it('can read from the read-only collection', async () => {
    let db = firebase.initializeTestApp({ projectId: MY_PROJECT_ID}).firestore() 
    let testDoc = db.collection('readonly')
    await firebase.assertFails(testDoc.add({}))
  })
})